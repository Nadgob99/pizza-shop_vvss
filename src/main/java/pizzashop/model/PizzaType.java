package pizzashop.model;

public enum PizzaType {
    Hawaii, Margherita, Funghi, Capricciosa, Quattro_Stagioni, Marinara, Calzone, Rucola, Napolitana
}
