package pizzashop.repository;

import pizzashop.model.MenuDataModel;
import pizzashop.model.PaymentType;
import pizzashop.model.PizzaType;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class MenuRepository {
    private static String filename = "D:\\Sem6\\VVSS\\pizza-shop_vvss\\src\\main\\resources\\data\\menu.txt";
    private List<MenuDataModel> listMenu;

    public MenuRepository(){
    }

    private void readMenu(){
        ClassLoader classLoader = MenuRepository.class.getClassLoader();
        this.listMenu= new ArrayList();
        BufferedReader br = null;
        try {
            FileReader file = new FileReader(filename);
            br = new BufferedReader(file);
            String line = null;
            while((line=br.readLine())!=null){
                MenuDataModel menuItem=getMenuItem(line);
                listMenu.add(menuItem);
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private MenuDataModel getMenuItem(String line){
        MenuDataModel item=null;
        if (line==null|| line.equals("")) return null;
        StringTokenizer st=new StringTokenizer(line, ",");
        String pizza= st.nextToken();
        double price = Double.parseDouble(st.nextToken());
        item = new MenuDataModel(0, price, PizzaType.valueOf(pizza));
        return item;
    }

    public List<MenuDataModel> getMenu(){
        readMenu();//create a new menu for each table, on request
        return listMenu;
    }

}
