package pizzashop.repository;

import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.validator.PaymentException;
import pizzashop.validator.PaymentValidator;

import java.util.ArrayList;
import java.util.List;

public class PaymentRepositoryInMemory {
    private List<Payment> paymentList;
    private PaymentValidator paymentValidator;

    public PaymentRepositoryInMemory(PaymentValidator validator) throws PaymentException {
        this.paymentList = new ArrayList<>();
        this.paymentValidator = validator;
        populate();
    }

    private void populate() throws PaymentException {
//        this.add(new Payment(4, PaymentType.Card, 10));
//        this.add(new Payment(5, PaymentType.Card, 10));
//        this.add(new Payment(6, PaymentType.Card, 10));
//        this.add(new Payment(7, PaymentType.Card, 10));
//        this.add(new Payment(3, PaymentType.Card, 10));
    }

    public void add(Payment payment) throws PaymentException {
        if (paymentValidator.validatePayment(payment))
            paymentList.add(payment);
        else
            throw new PaymentException("Invalid payment!");
    }

    public List<Payment> getAll(){
        return paymentList;
    }

}
