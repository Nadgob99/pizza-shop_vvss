package pizzashop.service;

import pizzashop.model.MenuDataModel;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.validator.PaymentException;
import pizzashop.validator.PaymentValidator;

import java.util.List;

public class PizzaService {

    private MenuRepository menuRepo;
    private PaymentRepository payRepo;
    private PaymentValidator validator;

    public PizzaService(MenuRepository menuRepo, PaymentRepository payRepo){
        this.menuRepo=menuRepo;
        this.payRepo=payRepo;
        this.validator = new PaymentValidator();
    }

    public List<MenuDataModel> getMenuData(){return menuRepo.getMenu();}

    public List<Payment> getPayments(){return payRepo.getAll(); }

    public void addPayment(int table, PaymentType type, double amount) throws PaymentException {
        Payment payment= new Payment(table, type, amount);
        if (validator.validatePayment(payment))
            payRepo.add(payment);
        else
            throw new PaymentException("Invalid payment!");
    }

    public double getTotalAmount(PaymentType type){
        double total=0.0f;
        List<Payment> l=getPayments();
        if (l==null)
            return total;
        if(l.isEmpty())
            return total;
        int index=0;
        while (index < l.size()){
            if (l.get(index).getType().equals(type))
                total+=l.get(index).getAmount();
            index += 1;
        }
        return total;
    }

    public void addInMemory(Payment payment){
            payRepo.addInMemory(payment);
    }

}
