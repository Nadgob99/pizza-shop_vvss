package pizzashop.validator;

public class PaymentException extends Exception {
    public PaymentException(String message) {
        super(message);
    }
}
