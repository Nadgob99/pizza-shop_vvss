package pizzashop.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pizzashop.model.Payment;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.validator.PaymentValidator;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

class PizzaServiceTestStep2 {

    private PaymentRepository paymentRepository;
    private PizzaService service;
    private MenuRepository menuRepository;

    @Mock
    private PaymentValidator paymentValidator;

    @Mock
    private Payment payment;
    @Mock
    private Payment payment2;
    @Mock
    private Payment payment3;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        menuRepository = new MenuRepository();
        paymentRepository = new PaymentRepository();
        service = new PizzaService(menuRepository, paymentRepository);
    }

    @Test
    void getPayments() {
        Mockito.when(paymentValidator.validatePayment(any())).thenReturn(true);
        service.addInMemory(payment);
        service.addInMemory(payment2);
        service.addInMemory(payment3);
        assertEquals(6, service.getPayments().size());
    }

    @Test
    void addPayment() {
        Mockito.when(paymentValidator.validatePayment(payment)).thenReturn(true);
        service.addInMemory(payment);
        service.addInMemory(payment);
        assertEquals(5, service.getPayments().size());
    }
}