package pizzashop.service;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.jupiter.api.Tag;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.validator.PaymentException;
import pizzashop.validator.PaymentValidator;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PizzaServiceTest {


    @Mock
    private PaymentRepository mockPaymentRepository;

    @InjectMocks
    private PizzaService service;

    @Before
    public void setUp() {
        MenuRepository menuRepository = new MenuRepository();
        mockPaymentRepository = Mockito.mock(PaymentRepository.class);
        service = new PizzaService(menuRepository, mockPaymentRepository);
    }

    @Tag("F02_TC01")
    @Test
    public void testGetTotalPaymentWithEmptyList() {
        when(mockPaymentRepository.getAll()).thenReturn(Collections.emptyList());

        double amount = service.getTotalAmount(PaymentType.Cash);
        assertEquals(amount, 0, 0);
    }

    @Tag("F02_TC02")
    @Test
    public void testGetTotalPaymentWithNullList() {
        when(mockPaymentRepository.getAll()).thenReturn(null);

        double amount = service.getTotalAmount(PaymentType.Cash);
        assertEquals(amount, 0, 0);
    }


    @Tag("F02_TC03")
    @Test
    public void testGetTotalPaymentArrayHavingNoPaymentOfType() {
        when(mockPaymentRepository.getAll()).thenReturn(List.of(new Payment(8, PaymentType.Card, 10)));

        double amount = service.getTotalAmount(PaymentType.Cash);
        assertEquals(amount, 0, 0);
    }


    @Test
    public void testGetTotalPayment() {
        when(mockPaymentRepository.getAll()).thenReturn(List.of(new Payment(8, PaymentType.Cash, 10)));

        double amount = service.getTotalAmount(PaymentType.Cash);
        assertEquals(amount, 10, 0);
    }

    @Tag("F02_TC05")
    @Test
    public void testGetTotalPaymentWithMixedTypes() {
        when(mockPaymentRepository.getAll()).thenReturn(List.of(new Payment(8, PaymentType.Cash, 10),
                new Payment(2, PaymentType.Card, 10),
                new Payment(4, PaymentType.Card, 10)));

        double amount = service.getTotalAmount(PaymentType.Card);
        assertEquals(amount, 20, 0);
    }


    @Test
    @Tag("bva")
    public void addValidPayment() {
        Payment payment = new Payment(1, PaymentType.Card, 100);

        assertDoesNotThrow( () -> {
            service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount());
        });
    }


    @Test
    @Tag("bva")
    public void addInvalidPayment() {

        Payment payment = new Payment(0, PaymentType.Card, 100);
        assertThrows(PaymentException.class, () -> {
            service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount());
        });
    }


    @Test
    @Tag("ecp")
    public void addValidPaymentEcp1() {
        Payment payment = new Payment(5, PaymentType.Card, 35);

        assertDoesNotThrow( () -> {
            service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount());
        });
    }

    @Test
    @Tag("ecp")
    public void addInvalidPaymentEcp1() {
        Payment payment = new Payment(5, PaymentType.Card, -35);
        assertThrows(PaymentException.class, () -> {
            service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount());
        });
    }


    @Test
    @Tag("ecp")
    public void addValidPaymentEcp3() {
        Payment payment = new Payment(5, PaymentType.Cash, 35);

        assertDoesNotThrow( () -> {
            service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount());
        });
    }

    @Test
    @Tag("ecp")
    public void addInvalidPaymentEcp4() {
        assertThrows(IllegalArgumentException.class, () -> {
            Payment payment = new Payment(5, PaymentType.valueOf("Cartof"), -35);
            service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount());
        });
    }


    @Test
    @Ignore
    public void test(){
        Payment payment = new Payment(5, PaymentType.Cash, 35);

        assertDoesNotThrow( () -> {
            service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount());
        });
    }

}