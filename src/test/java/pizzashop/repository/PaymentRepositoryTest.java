package pizzashop.repository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pizzashop.model.Payment;
import pizzashop.validator.PaymentException;
import pizzashop.validator.PaymentValidator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

class PaymentRepositoryTest {

    @InjectMocks
    private PaymentRepositoryInMemory repo;

    @Mock
    private PaymentValidator validator;

    private Payment payment;

    @BeforeEach
    public void setUp(){

        MockitoAnnotations.initMocks(this);

        payment=mock(Payment.class);
    }

    @Test
    public void add_test1(){
        Mockito.when(validator.validatePayment(payment)).thenReturn(false);

        try {
            repo.add(payment);
        } catch (PaymentException e) {
            assertEquals(0,repo.getAll().size());
        }

        Mockito.verify(validator, times(1)).validatePayment(payment);

    }

    @Test
    public void add_test2() {
        Mockito.when(validator.validatePayment(payment)).thenReturn(true);

        try {
            repo.add(payment);
            assertEquals( 1 , repo.getAll().size());
        } catch (PaymentException e) {
        }

        Mockito.verify(validator, times(1)).validatePayment(payment);
    }

    @AfterEach
    public void tearDown(){
        repo = null;
        payment=null;
    }
}