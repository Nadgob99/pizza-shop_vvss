package pizzashop.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PaymentTest {

    @Test
    void getTableNumber() {
        Payment p1 = new Payment(8,PaymentType.Card,10);
        assertEquals(8, p1.getTableNumber());
    }

    @Test
    void setTableNumber() {
        Payment p1 = new Payment(6,PaymentType.Card,10);
        p1.setTableNumber(8);
        assertEquals(8, p1.getTableNumber());
    }
}